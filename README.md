# socket-io-server #

[ ![Codeship Status for dougkiser/socket-io-server](https://app.codeship.com/projects/b0998450-390d-0135-cb5b-56b942729e0c/status?branch=master)](https://app.codeship.com/projects/228096)

An express and socket-io server starter

### What is this repository for? ###

* To help you get started quicker on your next socket-io project

### How do I get set up? ###

* yarn or npm i
* yarn start or npm start